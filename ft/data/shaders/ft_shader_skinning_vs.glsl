#version 330 core

const int MAX_BONES = 100;
const int MAX_BONES_PER_VERTICES = 4;

in vec3 a_position;
in vec3 a_normal;
in vec3 a_uv;
in vec3 a_color;
in int a_bone_ids[MAX_BONES_PER_VERTICES];
in float a_bone_weights[MAX_BONES_PER_VERTICES];

out vec3 frag_normal;
out vec3 frag_uv;
out vec3 frag_color;

uniform mat4 u_projection;
uniform mat4 u_view;
uniform mat4 u_model;
uniform mat4 u_bones[MAX_BONES];

void main()
{
	mat4 bone_transform = u_bones[a_bone_ids[0]] * a_bone_weights[0];
    bone_transform += u_bones[a_bone_ids[1]] * a_bone_weights[1];
    bone_transform += u_bones[a_bone_ids[2]] * a_bone_weights[2];
    bone_transform += u_bones[a_bone_ids[3]] * a_bone_weights[3];

	mat4 mvp = u_projection * u_view * u_model;
	//vec4 posl = bone_transform * vec4(a_position, 1.0f);

	vec4 posl = vec4(a_position, 1.0f);
	gl_Position = mvp * posl;	


	//gl_Position = mvp * bone_transform * vec4(a_position, 1.0f);
	frag_color = a_color;
	frag_uv = a_uv;
	frag_normal = mat3(transpose(inverse(u_model))) * a_normal;
}