#version 330 core

in vec3 a_position;
in vec3 a_normal;

uniform vec4 u_color;

out vec4 frag_color;

uniform mat4 u_mvp;

void main()
{
	gl_Position = u_mvp * vec4(a_position, 1.0f);
	frag_color = u_color;
}