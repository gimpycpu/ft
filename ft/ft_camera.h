#pragma once

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective

class ft_camera
{
	glm::vec3 m_position;
	float m_horizontal_angle;
	float m_vertical_angle;
	float m_fov;
	float m_near_plane;
	float m_far_plane;
	float m_aspect_ratio;
	float m_sensibility;

public:
	ft_camera() {};
	ft_camera(const glm::vec3& m_position, float m_horizontal_angle, float m_vertical_angle, float m_fov, float m_near_plane, float m_far_plane, float m_aspect_ratio);

	glm::vec3 up() const;
	glm::vec3 forward() const;
	glm::vec3 right() const;
	glm::mat4 orientation() const;
	void rotate(float horizontal_offset, float vertical_offset);
	glm::mat4 view() const;
	void move(const glm::vec3& offset);
	void set_sensibility(float sensibility) { m_sensibility = sensibility; };

	void normalize_angles();

	glm::mat4 projection() const;
	glm::mat4 matrix() const;
};
