#pragma once



/*
Simple vertex shader with only a vertex position
*/
const char* simple_vertex_shader = R"str(

#version 330 core

in vec3 in_vertex_pos_modelspace;
in vec3 in_color;

out vec3 frag_color;

uniform mat4 uni_mvp;

void main()
{
	gl_Position = uni_mvp * vec4(in_vertex_pos_modelspace, 1.0f);
	frag_color = in_color;
}

)str";

const char* g_skinning_vertex_shader = R"str(

#version 330 core

const int MAX_BONES = 100;
const int MAX_BONES_PER_VERTICES = 4;

in vec3 in_vertex_pos_modelspace;
in vec3 in_vertex_normal;
in vec3 in_vertex_uv;
in vec3 in_vertex_color;
in int in_vertex_bones_ids[MAX_BONES_PER_VERTICES];
in float in_vertex_bones_weights[MAX_BONES_PER_VERTICES];

out vec3 frag_normal;
out vec3 frag_uv;
out vec3 frag_color;

uniform mat4 uni_projection;
uniform mat4 uni_view;
uniform mat4 uni_model;
uniform mat4 uni_bones[MAX_BONES];

void main()
{
	mat4 bone_transform = uni_bones[in_vertex_bones_ids[0]] * in_vertex_bones_weights[0];
    bone_transform += uni_bones[in_vertex_bones_ids[1]] * in_vertex_bones_weights[1];
    bone_transform += uni_bones[in_vertex_bones_ids[2]] * in_vertex_bones_weights[2];
    bone_transform += uni_bones[in_vertex_bones_ids[3]] * in_vertex_bones_weights[3];

	mat4 mvp = uni_projection * uni_view * uni_model;
	vec4 posl = bone_transform * vec4(in_vertex_pos_modelspace, 1.0f);
	gl_Position = mvp * posl;	


	//gl_Position = mvp * bone_transform * vec4(in_vertex_pos_modelspace, 1.0f);
	frag_color = in_vertex_color;
	frag_uv = in_vertex_uv;
	frag_normal = mat3(transpose(inverse(uni_model))) * in_vertex_normal;
}

)str";

const char* g_skinning_fragment_shader = R"str(

#version 330 core

in vec3 frag_normal;

out vec4 out_color;

void main()
{
	out_color = vec4(frag_normal, 1.0f);
}

)str";

/*
Simply draw white
*/
const char* simple_fragment_shader = R"str(

#version 330 core
out vec4 out_color;

void main()
{
	out_color = vec4(1.0f, 1.0f, 1.0f, 1.0f);
}

)str";

/*
Simply draw white
*/
const char* simple_color_fragment_shader = R"str(

#version 330 core

in vec3 frag_color;

out vec3 out_color;

void main()
{
	out_color = frag_color;
}

)str";

const char* g_line_vertex_shader = R"str(

#version 150

in vec3 position;
in vec3 color;

uniform mat4 uni_view_proj;

out vec4 fragColor;

void main()
{

	gl_Position = uni_view_proj * vec4(position, 1.0f);
	fragColor = vec4(color, 1.0f);
}

)str";

const char* g_line_fragment_shader = R"str(

#version 150

in vec4 fragColor;
out vec4 out_color;

void main()
{
	out_color = fragColor;
}

)str";