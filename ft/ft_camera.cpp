#include "ft_camera.h"

#include <glm/vec4.hpp>

using glm::vec3;
using glm::vec4;
using glm::mat4;

static const float MaxVerticalAngle = 85.0f; //must be less than 90 to avoid gimbal lock

ft_camera::ft_camera(const vec3& position, float horizontal_angle, float vertical_angle, float fov, float near_plane, float far_plane, float aspect_ratio)
{
	m_position = position;
	m_horizontal_angle = horizontal_angle;
	m_vertical_angle = vertical_angle;
	m_fov = fov;
	m_near_plane = near_plane;
	m_far_plane = far_plane;
	m_aspect_ratio = aspect_ratio;https://www.tradingview.com/x/BCpNJvsC/
	m_sensibility = 1.0f;
	normalize_angles();
}

mat4 ft_camera::projection() const
{
	return glm::perspective(m_fov, m_aspect_ratio, m_near_plane, m_far_plane);
}

mat4 ft_camera::view() const
{
	return orientation() * glm::translate(mat4(), -m_position);
}

mat4 ft_camera::matrix() const
{
	return projection() * view();
}

vec3 ft_camera::up() const
{
	vec4 up = glm::inverse(orientation()) * vec4(0.0f, 1.0f, 0.0f, 1.0f);
	return vec3(up);
}

vec3 ft_camera::right() const
{
	vec4 right = glm::inverse(orientation()) * vec4(1.0f, 0.0f, 0.0f, 1.0f);
	return vec3(right);
}

vec3 ft_camera::forward() const
{
	vec4 forward = glm::inverse(orientation()) * vec4(0.0f, 0.0f, -1.0f, 1.0f);
	return vec3(forward);
}

mat4 ft_camera::orientation() const
{
	mat4 mat;
	mat = glm::rotate(mat, glm::radians(m_vertical_angle), vec3(1.0f, 0.0f, 0.0f));
	mat = glm::rotate(mat, glm::radians(m_horizontal_angle), vec3(0.0f, 1.0f, 0.0f));
	return mat;
}

void ft_camera::rotate(float horizontal_offset, float vertical_offset)
{
	m_vertical_angle += m_sensibility * vertical_offset;
	m_horizontal_angle += m_sensibility * horizontal_offset;
	normalize_angles();
}

void ft_camera::move(const vec3& offset)
{
	m_position += offset;
}

void ft_camera::normalize_angles() {
	m_horizontal_angle = fmodf(m_horizontal_angle, 360.0f);
	//fmodf can return negative values, but this will make them all positive
	if (m_horizontal_angle < 0.0f)
		m_horizontal_angle += 360.0f;

	if (m_vertical_angle > MaxVerticalAngle)
		m_vertical_angle = MaxVerticalAngle;
	else if (m_vertical_angle < -MaxVerticalAngle)
		m_vertical_angle = -MaxVerticalAngle;
}