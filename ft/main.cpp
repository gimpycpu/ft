#include <stdio.h>

#define SDL_MAIN_HANDLED
#include <GL\gl3w.h>
#include <SDL\SDL.h>


#include <glm\glm.hpp>
#include <glm/gtc/type_ptr.hpp> //value_ptr
#include <glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective
#include <glm/gtx/quaternion.hpp> //glm::toMat4

#include "imgui.h"
#include "imgui_impl_sdl_gl3.h"

#include "ft_shaders.h"

#define FT_OFFSET_OF(type, member) ((void*)&(((type *)0)->member))
#define FT_ARRAY_COUNT(arr) (sizeof(arr) / sizeof(*arr))

#ifdef FT_DEBUG
#define FT_ASSERT(exp, ...) if(!(exp)) { __debugbreak(); }
#define FT_DEBUG_CODE(code) code
#define FT_PRINTF printf
#define FT_FPRINTF fprintf
#define FT_GL_ERROR_CHECK(code) (code); { GLenum error = glGetError(); FT_ASSERT(error == GL_NO_ERROR, "GL_ERROR") };
#else
#define FT_ASSERT(exp, ...) (exp)
#define FT_DEBUG_CODE(code)
#define FT_PRINTF(void)
#define FT_FPRINTF(void)
#define FT_GL_ERROR_CHECK(code) (code);
#endif

union ft_float2
{
	struct { float x, y; };
	float data[2];
};

union ft_float3
{
	struct { float x, y, z; };
	float data[3];
};

union ft_float4
{
	struct { float x, y, z, w; };
	float data[4];
};

union ft_mat4
{
	ft_float4 rows[4];
	float data[16];
};

enum ft_render_material_flag
{
	FT_RENDER_MATERIAL_COLOR = 1 << 0,
};

struct ft_render_material
{
	ft_float4 diffuse;

	ft_render_material_flag material_flag;
};

struct ft_mesh_triangle_list
{
	uint32_t num_indices;
	uint32_t first_index;
};

#define FT_MAX_BONE_PER_VERTEX 4
struct ft_vertex_bone_data
{
	uint32_t bone_indices[FT_MAX_BONE_PER_VERTEX];
	float bone_weights[FT_MAX_BONE_PER_VERTEX];
};

struct ft_vertex
{
	ft_float3 position;
	ft_float3 normal;
	ft_float2 uv;

	uint32_t boneIndices[FT_MAX_BONE_PER_VERTEX];
	float boneWeights[FT_MAX_BONE_PER_VERTEX];
};

struct ft_mesh_joint
{
	char name[255];
	uint32_t parentIndex;
	ft_mat4 inverseBindPose;
};

struct ft_mesh
{
	uint32_t num_vertices;
	uint32_t num_indices;
	uint32_t num_triangle_lists;
	uint32_t num_joints;

	ft_vertex* vertices;
	uint32_t* indices;
	ft_mesh_triangle_list* triangleLists;
	ft_mesh_joint* joints;

	//FIXME temp stuff until I found somewhere else
	unsigned int vao;
	ft_render_material render_material;
};

struct ft_mesh_header
{
	uint32_t version;
	uint32_t numVertices;
	uint32_t numIndices;
	uint32_t numTriangleLists;
	uint32_t numJoints;
};

enum ft_opengl_shader_location
{
	FT_OPENGL_SHADER_ATTRIB_LOC_POSITION,
	FT_OPENGL_SHADER_ATTRIB_LOC_COLOR,
	FT_OPENGL_SHADER_ATTRIB_LOC_NORMAL,
	FT_OPENGL_SHADER_ATTRIB_LOC_UV,
	FT_OPENGL_SHADER_ATTRIB_LOC_BONE_IDS,
	FT_OPENGL_SHADER_ATTRIB_LOC_BONE_WEIGHTS,

	FT_OPENGL_SHADER_ATTRIB_LOC_COUNT
};

enum ft_opengl_shader_uniform_location
{
	FT_OPENGL_SHADER_UNIFORM_LOC_MVP,
	FT_OPENGL_SHADER_UNIFORM_LOC_MODEL,
	FT_OPENGL_SHADER_UNIFORM_LOC_VIEW,
	FT_OPENGL_SHADER_UNIFORM_LOC_PROJECTION,
	FT_OPENGL_SHADER_UNIFORM_LOC_BONES,
	FT_OPENGL_SHADER_UNIFORM_COLOR,

	FT_OPENGL_SHADER_UNIFORM_LOC_COUNT
};

static const char* ft_shader_attrib_names[] =
{
	"a_position",
	"a_color",
	"a_normal",
	"a_uv",
	"a_bone_ids",
	"a_bone_weights"
};

static const char* ft_shader_uniform_names[] =
{
	"u_mvp",
	"u_model",
	"u_view",
	"u_projection",
	"u_bones",
	"u_color"
};

struct ft_gl_shader_program
{
	GLuint vertex_shader;
	GLuint fragment_shader;
	GLuint handle;

	GLint attrib_locations[FT_OPENGL_SHADER_ATTRIB_LOC_COUNT];
	GLint uniform_locations[FT_OPENGL_SHADER_UNIFORM_LOC_COUNT];
};

void ft_platform_read_file(const char* path, char* buffer, int32_t *size)
{
	if (FILE* file = fopen(path, "rb"))
	{
		fseek(file, 0, SEEK_END);
		int32_t byte_read = ftell(file);
		fseek(file, 0, SEEK_SET);
		fread(buffer, sizeof(*buffer), byte_read, file);
		buffer[byte_read++] = '\0';
		fclose(file);
		if (size)
		{
			*size = byte_read;
		}
	}
}

void ft_platform_file_read_alloc(const char* path, char** buffer, int32_t *size)
{
	if (FILE* file = fopen(path, "rb"))
	{
		fseek(file, 0, SEEK_END);
		int32_t byte_read = ftell(file);
		char* memory = (char*)malloc(byte_read + 1);
		fseek(file, 0, SEEK_SET);
		fread(memory, sizeof(*memory), byte_read, file);
		memory[byte_read++] = '\0';
		fclose(file);
		*buffer = memory;
		if (size)
		{
			*size = byte_read;
		}
	}
}

void ft_platform_free(void* memory)
{
	free(memory);
}

int ft_gl_create_program(const char* vertex_shader_src, const char* fragment_shader_src, ft_gl_shader_program* out)
{
	*out = {};

	GLuint program = FT_GL_ERROR_CHECK(glCreateProgram());
	GLuint vertex_shader = FT_GL_ERROR_CHECK(glCreateShader(GL_VERTEX_SHADER));
	GLuint fragment_shader = FT_GL_ERROR_CHECK(glCreateShader(GL_FRAGMENT_SHADER));

	GLint status;

	FT_GL_ERROR_CHECK(glShaderSource(vertex_shader, 1, &vertex_shader_src, NULL));
	FT_GL_ERROR_CHECK(glCompileShader(vertex_shader));
	FT_GL_ERROR_CHECK(glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &status));
	if (!status)
	{
		GLchar buf[1024];
		GLsizei len;
		FT_GL_ERROR_CHECK(glGetShaderInfoLog(vertex_shader, 1024, &len, buf));
		printf("Vertex Shader compile error: %s\n", buf);

		return -1;
	}

	FT_GL_ERROR_CHECK(glShaderSource(fragment_shader, 1, &fragment_shader_src, NULL));
	FT_GL_ERROR_CHECK(glCompileShader(fragment_shader));
	FT_GL_ERROR_CHECK(glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &status));
	if (!status)
	{
		GLchar buf[1024];
		GLsizei len;
		FT_GL_ERROR_CHECK(glGetShaderInfoLog(fragment_shader, 1024, &len, buf));
		printf("Fragment Shader compile error: %s\n", buf);

		return -1;
	}

	FT_GL_ERROR_CHECK(glAttachShader(program, vertex_shader));
	FT_GL_ERROR_CHECK(glAttachShader(program, fragment_shader));
	FT_GL_ERROR_CHECK(glLinkProgram(program));

	for (size_t i = 0; i < FT_OPENGL_SHADER_ATTRIB_LOC_COUNT; i++)
	{
		const char* attrib_name = ft_shader_attrib_names[i];
		out->attrib_locations[i] = FT_GL_ERROR_CHECK(glGetAttribLocation(program, attrib_name));
	}

	for (size_t i = 0; i < FT_OPENGL_SHADER_UNIFORM_LOC_COUNT; i++)
	{
		const char* uniform_name = ft_shader_uniform_names[i];
		out->uniform_locations[i] = FT_GL_ERROR_CHECK(glGetUniformLocation(program, uniform_name));
	}

	out->fragment_shader = fragment_shader;
	out->vertex_shader = vertex_shader;
	out->handle = program;

	return 0;
}

int ft_gl_shader_program_load(const char* vertex_shader_path, const char* fragment_shader_path, ft_gl_shader_program* out)
{
	char* vertex_shader_src = NULL;
	char* fragment_shader_src = NULL;
	int error = 0;

	ft_platform_file_read_alloc(vertex_shader_path, &vertex_shader_src, NULL);
	ft_platform_file_read_alloc(fragment_shader_path, &fragment_shader_src, NULL);

	if (vertex_shader_src && fragment_shader_src)
	{
		error = ft_gl_create_program(vertex_shader_src, fragment_shader_src, out);
	}

	ft_platform_free(vertex_shader_src);
	ft_platform_free(fragment_shader_src);

	return error;
}

ft_mesh* load_ft_mesh(const char* filename)
{
	ft_mesh* mesh = NULL;
	if (FILE* file = fopen(filename, "rb"))
	{
		ft_mesh_header header = {};
		fread(&header, sizeof(header), 1, file);

		size_t total_memory = sizeof(header)
			+ sizeof(ft_vertex) * header.numVertices
			+ sizeof(uint32_t) * header.numIndices
			+ sizeof(ft_mesh_triangle_list) * header.numTriangleLists
			+ sizeof(ft_mesh_joint) * header.numJoints
			+ sizeof(ft_vertex_bone_data) * header.numVertices;

		char* memory = (char*)malloc(total_memory);
		memset(memory, 0, total_memory);

		mesh = (ft_mesh*)memory;
		mesh->num_indices = header.numIndices;
		mesh->num_triangle_lists = header.numTriangleLists;
		mesh->num_vertices = header.numVertices;
		mesh->num_joints = header.numJoints;

		mesh->vertices = (ft_vertex*)(memory + sizeof(*mesh));
		mesh->indices = (uint32_t*)((char*)mesh->vertices + sizeof(ft_vertex) * header.numVertices);
		mesh->triangleLists = (ft_mesh_triangle_list*)((char*)mesh->indices + sizeof(uint32_t) * header.numIndices);
		mesh->joints = (ft_mesh_joint*)((char*)mesh->triangleLists + sizeof(ft_vertex) * header.numTriangleLists);

		fread(mesh->vertices, sizeof(*mesh->vertices), mesh->num_vertices, file);
		fread(mesh->indices, sizeof(*mesh->indices), mesh->num_indices, file);
		fread(mesh->triangleLists, sizeof(*mesh->triangleLists), mesh->num_triangle_lists, file);
		fread(mesh->joints, sizeof(*mesh->joints), mesh->num_joints, file);

		fclose(file);
	}
	return mesh;
}

void prepare_mesh_for_render(ft_mesh* mesh, ft_gl_shader_program* program)
{
	GLuint program_name = program->handle;
	FT_GL_ERROR_CHECK(glUseProgram(program_name));
	GLuint vao;

	//TODO 1 vao per mesh for now
	FT_GL_ERROR_CHECK(glGenVertexArrays(1, &vao));
	FT_GL_ERROR_CHECK(glBindVertexArray(vao));

	GLuint vbo;
	FT_GL_ERROR_CHECK(glGenBuffers(1, &vbo));

	FT_GL_ERROR_CHECK(glBindBuffer(GL_ARRAY_BUFFER, vbo));
	FT_GL_ERROR_CHECK(glBufferData(GL_ARRAY_BUFFER, sizeof(*mesh->vertices) * mesh->num_vertices, mesh->vertices, GL_STATIC_DRAW));

	GLsizei stride = sizeof(*mesh->vertices);

	FT_ASSERT(0);
	GLint vert_pos_loc = program->attrib_locations[FT_OPENGL_SHADER_ATTRIB_LOC_POSITION];
	GLint vert_normal_loc = program->attrib_locations[FT_OPENGL_SHADER_ATTRIB_LOC_NORMAL];

	//GLint vert_bones_ids_loc = FT_GL_ERROR_CHECK(glGetAttribLocation(program_name, "in_vertex_bones_ids"));
	//GLint vert_bones_weights_loc = FT_GL_ERROR_CHECK(glGetAttribLocation(program_name, "in_vertex_bones_weights"));

	FT_GL_ERROR_CHECK(glEnableVertexAttribArray(vert_pos_loc));
	FT_GL_ERROR_CHECK(glVertexAttribPointer(vert_pos_loc, 3, GL_FLOAT, GL_FALSE, stride, FT_OFFSET_OF(ft_vertex, position)));

	FT_GL_ERROR_CHECK(glEnableVertexAttribArray(vert_normal_loc));
	FT_GL_ERROR_CHECK(glVertexAttribPointer(vert_normal_loc, 3, GL_FLOAT, GL_FALSE, stride, FT_OFFSET_OF(ft_vertex, normal)));

	//FT_GL_ERROR_CHECK(glEnableVertexAttribArray(vert_bones_ids_loc));
	//FT_GL_ERROR_CHECK(glVertexAttribIPointer(vert_bones_ids_loc, 4, GL_UNSIGNED_SHORT, stride, FT_OFFSET_OF(Vertex, bone_indices)));

	//FT_GL_ERROR_CHECK(glEnableVertexAttribArray(vert_bones_weights_loc));
	//FT_GL_ERROR_CHECK(glVertexAttribPointer(vert_bones_weights_loc, 4, GL_FLOAT, GL_FALSE, stride, FT_OFFSET_OF(Vertex, bone_weights)));

	GLuint index_buffer;
	FT_GL_ERROR_CHECK(glCreateBuffers(1, &index_buffer));
	FT_GL_ERROR_CHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer));
	FT_GL_ERROR_CHECK(glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh->num_indices * sizeof(*mesh->indices), mesh->indices, GL_STATIC_DRAW));

	mesh->vao = vao;
}

void debugDrawTriangle()
{
	static GLuint vao;
	static int firstPass = 1;
	static const GLfloat vertexBufferData[][3] = {
		{ -1.0f, -1.0f, 0.0f },
		{ 1.0f, -1.0f, 0.0f },
		{ 0.0f,  1.0f, 0.0f },
	};
	static GLuint program;
	static glm::mat4 projection;
	static glm::mat4 view;
	static glm::mat4 model;
	if (firstPass)
	{
		const char* vertexShaderSrc = R"(
			#version 330 core

			in vec3 aVertPos;

			uniform mat4 uMvp;

			void main()
			{
				gl_Position = uMvp * vec4(aVertPos, 1.0f);
			}

		)";

		const char* fragmentShaderSrc = R"(

			#version 330 core

			out vec4 finalColor;

			void main()
			{
				finalColor = vec4(1.0f, 1.0f, 1.0f, 1.0f);
			}

		)";

		program = FT_GL_ERROR_CHECK(glCreateProgram());
		GLuint vertexShader = FT_GL_ERROR_CHECK(glCreateShader(GL_VERTEX_SHADER));
		GLuint fragmentShader = FT_GL_ERROR_CHECK(glCreateShader(GL_FRAGMENT_SHADER));

		FT_GL_ERROR_CHECK(glShaderSource(vertexShader, 1, &vertexShaderSrc, NULL));
		FT_GL_ERROR_CHECK(glCompileShader(vertexShader));

		FT_GL_ERROR_CHECK(glShaderSource(fragmentShader, 1, &fragmentShaderSrc, NULL));
		FT_GL_ERROR_CHECK(glCompileShader(fragmentShader));

		FT_GL_ERROR_CHECK(glAttachShader(program, vertexShader));
		FT_GL_ERROR_CHECK(glAttachShader(program, fragmentShader));
		FT_GL_ERROR_CHECK(glLinkProgram(program));

		FT_GL_ERROR_CHECK(glGenVertexArrays(1, &vao));
		FT_GL_ERROR_CHECK(glBindVertexArray(vao));

		GLuint vbo;
		FT_GL_ERROR_CHECK(glGenBuffers(1, &vbo));
		FT_GL_ERROR_CHECK(glBindBuffer(GL_ARRAY_BUFFER, vbo));

		GLuint vertPos = FT_GL_ERROR_CHECK(glGetAttribLocation(program, "aVertPos"));

		FT_GL_ERROR_CHECK(glEnableVertexAttribArray(vertPos));
		FT_GL_ERROR_CHECK(glVertexAttribPointer(vertPos, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 3, NULL));

		FT_GL_ERROR_CHECK(glBufferData(GL_ARRAY_BUFFER, sizeof(vertexBufferData), vertexBufferData, GL_STATIC_DRAW));

		projection = glm::perspective(120.0f, 1024.0f / 768.0f, 0.1f, 100.0f);

		glm::vec3 eye = glm::vec3(0.0f, 0.0f, 10.0f);
		glm::vec3 center;
		glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);

		view = glm::lookAt(eye, center, up);

		firstPass = 0;
	}
	else
	{
		FT_GL_ERROR_CHECK(glBindVertexArray(vao));
	}

	FT_GL_ERROR_CHECK(glUseProgram(program));

	glm::mat4 mvp = projection * view * model;

	static GLuint mvpPos = FT_GL_ERROR_CHECK(glGetUniformLocation(program, "uMvp"));
	FT_GL_ERROR_CHECK(glUniformMatrix4fv(mvpPos, 1, GL_FALSE, glm::value_ptr(mvp)));

	FT_GL_ERROR_CHECK(glDrawArrays(GL_TRIANGLES, 0, 3));

}

void debugGLCreateTriangleMesh(ft_gl_shader_program* program, ft_mesh* out_mesh)
{

	static GLuint vao;
	static const GLfloat vertex_buffer_data[][3] = {
		{ -1.0f, -1.0f, 0.0f },
		{ 1.0f, -1.0f, 0.0f },
		{ 0.0f,  1.0f, 0.0f },
	};
	static glm::mat4 projection;
	static glm::mat4 view;
	static glm::mat4 model;
		
	GLuint vertex_shader = FT_GL_ERROR_CHECK(glCreateShader(GL_VERTEX_SHADER));
	GLuint fragment_shader = FT_GL_ERROR_CHECK(glCreateShader(GL_FRAGMENT_SHADER));

	FT_GL_ERROR_CHECK(glGenVertexArrays(1, &vao));
	FT_GL_ERROR_CHECK(glBindVertexArray(vao));

	GLuint vbo;
	FT_GL_ERROR_CHECK(glGenBuffers(1, &vbo));
	FT_GL_ERROR_CHECK(glBindBuffer(GL_ARRAY_BUFFER, vbo));

	GLuint vertPos = FT_GL_ERROR_CHECK(glGetAttribLocation(program->handle, "aVertPos"));

	FT_GL_ERROR_CHECK(glEnableVertexAttribArray(program->attrib_locations[FT_OPENGL_SHADER_ATTRIB_LOC_POSITION]));
	FT_GL_ERROR_CHECK(glVertexAttribPointer(program->attrib_locations[FT_OPENGL_SHADER_ATTRIB_LOC_POSITION], 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 3, NULL));

	FT_GL_ERROR_CHECK(glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_buffer_data), vertex_buffer_data, GL_STATIC_DRAW));



	out_mesh->indices = NULL;
	out_mesh->vertices = NULL;
	out_mesh->joints = NULL;
	out_mesh->num_joints = 0;
	out_mesh->vertices = (ft_vertex*)malloc(sizeof(ft_vertex) * 3);
	memset(out_mesh->vertices, NULL, sizeof(ft_vertex) * 3);
	out_mesh->vertices[0].position = { -1.0f, -1.0f, 0.0f };
	out_mesh->vertices[1].position = { 1.0f, -1.0f, 0.0f };
	out_mesh->vertices[2].position = { 0.0f,  1.0f, 0.0f };
	out_mesh->indices = (uint32_t*)malloc(sizeof(uint32_t) * 3);
	out_mesh->indices[0] = 0;
	out_mesh->indices[1] = 1;
	out_mesh->indices[2] = 2;
	out_mesh->vao = vao;
	out_mesh->num_triangle_lists = 1;
	out_mesh->num_indices = 3;
	out_mesh->num_vertices = 3;
	out_mesh->triangleLists = (ft_mesh_triangle_list*)malloc(sizeof(ft_mesh_triangle_list));
	out_mesh->triangleLists[0].first_index = 0;
	out_mesh->triangleLists[0].num_indices = 3;
	out_mesh->render_material.diffuse = { 0.5f, 0.6f, 0.1f, 1.0f };
	out_mesh->render_material.material_flag = (ft_render_material_flag)(out_mesh->render_material.material_flag | FT_RENDER_MATERIAL_COLOR);

	GLuint index_buffer;
	FT_GL_ERROR_CHECK(glCreateBuffers(1, &index_buffer));
	FT_GL_ERROR_CHECK(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer));
	FT_GL_ERROR_CHECK(glBufferData(GL_ELEMENT_ARRAY_BUFFER, out_mesh->num_indices * sizeof(*out_mesh->indices), out_mesh->indices, GL_STATIC_DRAW));
}

void DEBUG_render_mesh(ft_mesh* mesh, ft_gl_shader_program* program, glm::mat4& camera )
{
	FT_GL_ERROR_CHECK(glUseProgram(program->handle));

	FT_GL_ERROR_CHECK(glUniformMatrix4fv(program->uniform_locations[FT_OPENGL_SHADER_UNIFORM_LOC_MVP], 1, GL_FALSE, glm::value_ptr(camera)));

	ft_render_material* render_material = &mesh->render_material;
	FT_GL_ERROR_CHECK(glUniform4f(program->uniform_locations[FT_OPENGL_SHADER_UNIFORM_COLOR], render_material->diffuse.x, render_material->diffuse.y, render_material->diffuse.z, render_material->diffuse.w));
	FT_GL_ERROR_CHECK(glBindVertexArray(mesh->vao));
	for (size_t i = 0; i < mesh->num_triangle_lists; i++)
	{
		FT_GL_ERROR_CHECK(glDrawElements(GL_TRIANGLES, mesh->triangleLists[i].num_indices, GL_UNSIGNED_INT, (void*)mesh->triangleLists[i].first_index));
	}

}

void DEBUG_draw_mesh(ft_mesh* mesh)
{
	ft_render_material* material = &mesh->render_material;
	ImGui::SliderFloat4("Diffuse", material->diffuse.data, 0.0f, 1.0f);
}

int main(int argc, char* argv[])
{
	SDL_SetMainReady();
	if (SDL_Init(SDL_INIT_VIDEO) != 0)
	{
		fprintf(stderr, "Unable to initialize SDL: %s", SDL_GetError());
		return -1;
	}

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	SDL_Window* window = SDL_CreateWindow("Hello Triangle", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1024, 768, SDL_WINDOW_OPENGL);
	if (!window)
	{
		fprintf(stderr, "SDL_CreateWindow: %s", SDL_GetError());
		return -1;
	}

	SDL_GLContext glContext = SDL_GL_CreateContext(window);
	SDL_GL_MakeCurrent(window, glContext);

	if (gl3wInit()) {
		fprintf(stderr, "failed to initialize OpenGL\n");
		return -1;
	}
	if (!gl3wIsSupported(3, 3)) {
		fprintf(stderr, "OpenGL 3.3 not supported\n");
		return -1;
	}
	const GLubyte* glVersion = FT_GL_ERROR_CHECK(glGetString(GL_VERSION));
	const GLubyte* glShadingLanguageVersion = glGetString(GL_SHADING_LANGUAGE_VERSION);

	printf("OpenGL %s, GLSL %s\n", glVersion, glShadingLanguageVersion);
	FT_GL_ERROR_CHECK(glClearColor(.2f, .2f, .2f, 0.0f));
	int isRunning = 1;

	ft_gl_shader_program skinningProgram;
	ft_gl_shader_program staticMeshProgram;
	ft_gl_shader_program triangle_program;
	ft_gl_shader_program_load("../shaders/ft_shader_skinning_vs.glsl", "../shaders/ft_shader_skinning_fs.glsl", &skinningProgram);
	ft_gl_shader_program_load("../shaders/ft_shader_static_mesh_vs.glsl", "../shaders/ft_shader_static_mesh_fs.glsl", &staticMeshProgram);
	ft_gl_shader_program_load("../shaders/ft_shader_triangle_vs.glsl", "../shaders/ft_shader_triangle_fs.glsl", &triangle_program);

	ImGui_ImplSdlGL3_Init(window);

	bool show_test_window = true;
	bool show_another_window = false;
	ImVec4 clear_color = ImColor(114, 144, 154);
	ft_mesh triangle_mesh = {};
	debugGLCreateTriangleMesh(&triangle_program, &triangle_mesh);

	static glm::mat4 projection;
	static glm::mat4 view;
	static glm::mat4 model;

	while (isRunning)
	{
		SDL_Event e;

		while (SDL_PollEvent(&e))
		{
			ImGui_ImplSdlGL3_ProcessEvent(&e);
			switch (e.type)
			{
			case SDL_KEYUP:
				if (e.key.keysym.scancode == SDL_SCANCODE_ESCAPE)
				{
					isRunning = 0;
				}
				break;
			case SDL_QUIT:
				isRunning = 0;
				break;
			default:
				break;
			}
		}

		ImGui_ImplSdlGL3_NewFrame(window);
		
		// 1. Show a simple window
		// Tip: if we don't call ImGui::Begin()/ImGui::End() the widgets appears in a window automatically called "Debug"
		{
			ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
		}

		// 2. Show another simple window, this time using an explicit Begin/End pair
		/*if (show_another_window)
		{
			ImGui::SetNextWindowSize(ImVec2(200, 100), ImGuiSetCond_FirstUseEver);
			ImGui::Begin("Another Window", &show_another_window);
			ImGui::Text("Hello");
			ImGui::End();
		}

		// 3. Show the ImGui test window. Most of the sample code is in ImGui::ShowTestWindow()
		if (show_test_window)
		{
			ImGui::SetNextWindowPos(ImVec2(650, 20), ImGuiSetCond_FirstUseEver);
			ImGui::ShowTestWindow(&show_test_window);
		}*/

		FT_GL_ERROR_CHECK(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));

		DEBUG_draw_mesh(&triangle_mesh);
		//debugDrawTriangle();

		glm::mat4 mvp = projection * view;

		DEBUG_render_mesh(&triangle_mesh, &triangle_program, mvp);

		ImGui::Render();
		SDL_GL_SwapWindow(window);
	}

	ImGui_ImplSdlGL3_Shutdown();
	SDL_GL_DeleteContext(glContext);
	SDL_DestroyWindow(window);
	SDL_Quit();
	return 0;
}